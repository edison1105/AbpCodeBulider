﻿namespace AbpCodeBulider.DxUI
{
    partial class FrmCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chk = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gvList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcList = new DevExpress.XtraGrid.GridControl();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkAdminPermission = new DevExpress.XtraEditors.CheckEdit();
            this.btnCodeGenPreview = new DevExpress.XtraEditors.SimpleButton();
            this.txtPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtEntityPluralName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.chkPk = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cbeBaseClass = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtTableName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtCompanyName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtEntityName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtNamespace = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.chkServer = new DevExpress.XtraEditors.CheckEdit();
            this.btnOpenSaveDir = new DevExpress.XtraEditors.SimpleButton();
            this.txtSaveCodeDir = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.btnOutput = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chkClient = new DevExpress.XtraEditors.CheckEdit();
            this.chkAppService = new DevExpress.XtraEditors.CheckEdit();
            this.chkAppServiceInterface = new DevExpress.XtraEditors.CheckEdit();
            this.chkExportingService = new DevExpress.XtraEditors.CheckEdit();
            this.chkExportingIntercafe = new DevExpress.XtraEditors.CheckEdit();
            this.chkCreateOrEditInput = new DevExpress.XtraEditors.CheckEdit();
            this.chkEntityDto = new DevExpress.XtraEditors.CheckEdit();
            this.chkGetAllForExcelInput = new DevExpress.XtraEditors.CheckEdit();
            this.chkGetAllInput = new DevExpress.XtraEditors.CheckEdit();
            this.chkGetForEditOutput = new DevExpress.XtraEditors.CheckEdit();
            this.chkDapperMapper = new DevExpress.XtraEditors.CheckEdit();
            this.chkAuthorizationProvider = new DevExpress.XtraEditors.CheckEdit();
            this.chkBatchDeleteInput = new DevExpress.XtraEditors.CheckEdit();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkAllDto = new DevExpress.XtraEditors.CheckEdit();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkLocalization = new DevExpress.XtraEditors.CheckEdit();
            this.chkExcelExport = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.chk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcList)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdminPermission.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntityPluralName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeBaseClass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTableName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntityName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamespace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkServer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaveCodeDir.Properties)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkClient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAppService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAppServiceInterface.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportingService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportingIntercafe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateOrEditInput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEntityDto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetAllForExcelInput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetAllInput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetForEditOutput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDapperMapper.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationProvider.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBatchDeleteInput.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllDto.Properties)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocalization.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcelExport.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // chk
            // 
            this.chk.AutoHeight = false;
            this.chk.Name = "chk";
            // 
            // gvList
            // 
            this.gvList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn8,
            this.gridColumn6,
            this.gridColumn9,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn7,
            this.gridColumn10,
            this.gridColumn11});
            this.gvList.GridControl = this.gcList;
            this.gvList.IndicatorWidth = 40;
            this.gvList.Name = "gvList";
            this.gvList.OptionsView.ColumnAutoWidth = false;
            this.gvList.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "状态";
            this.gridColumn1.ColumnEdit = this.chk;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "列名";
            this.gridColumn2.FieldName = "name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "类型";
            this.gridColumn3.FieldName = "Type";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "标识";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "主键";
            this.gridColumn6.FieldName = "IsKey";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 7;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "外键";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 4;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "长度";
            this.gridColumn4.FieldName = "Length";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "允许空";
            this.gridColumn5.FieldName = "IsNull";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "小数位数";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 8;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "默认值";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 9;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "字段说明";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 10;
            // 
            // gcList
            // 
            this.gcList.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcList.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(5);
            this.gcList.Location = new System.Drawing.Point(0, 0);
            this.gcList.MainView = this.gvList;
            this.gcList.Margin = new System.Windows.Forms.Padding(5);
            this.gcList.Name = "gcList";
            this.gcList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chk});
            this.gcList.Size = new System.Drawing.Size(1729, 516);
            this.gcList.TabIndex = 6;
            this.gcList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvList});
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl1.Location = new System.Drawing.Point(0, 516);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(5);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(1729, 9);
            this.splitterControl1.TabIndex = 4;
            this.splitterControl1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.btnCodeGenPreview);
            this.groupBox2.Controls.Add(this.txtPrefix);
            this.groupBox2.Controls.Add(this.labelControl9);
            this.groupBox2.Controls.Add(this.txtEntityPluralName);
            this.groupBox2.Controls.Add(this.labelControl8);
            this.groupBox2.Controls.Add(this.chkPk);
            this.groupBox2.Controls.Add(this.labelControl7);
            this.groupBox2.Controls.Add(this.cbeBaseClass);
            this.groupBox2.Controls.Add(this.labelControl6);
            this.groupBox2.Controls.Add(this.txtTableName);
            this.groupBox2.Controls.Add(this.labelControl5);
            this.groupBox2.Controls.Add(this.txtProjectName);
            this.groupBox2.Controls.Add(this.labelControl3);
            this.groupBox2.Controls.Add(this.txtCompanyName);
            this.groupBox2.Controls.Add(this.labelControl1);
            this.groupBox2.Controls.Add(this.txtEntityName);
            this.groupBox2.Controls.Add(this.labelControl4);
            this.groupBox2.Controls.Add(this.txtNamespace);
            this.groupBox2.Controls.Add(this.labelControl2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 705);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox2.Size = new System.Drawing.Size(1729, 157);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "实体信息";
            // 
            // chkAdminPermission
            // 
            this.chkAdminPermission.Location = new System.Drawing.Point(660, 104);
            this.chkAdminPermission.Margin = new System.Windows.Forms.Padding(5);
            this.chkAdminPermission.Name = "chkAdminPermission";
            this.chkAdminPermission.Properties.Caption = "Is Administration Permission";
            this.chkAdminPermission.Size = new System.Drawing.Size(243, 26);
            this.chkAdminPermission.TabIndex = 6;
            // 
            // btnCodeGenPreview
            // 
            this.btnCodeGenPreview.Location = new System.Drawing.Point(1026, 92);
            this.btnCodeGenPreview.Margin = new System.Windows.Forms.Padding(5);
            this.btnCodeGenPreview.Name = "btnCodeGenPreview";
            this.btnCodeGenPreview.Size = new System.Drawing.Size(125, 39);
            this.btnCodeGenPreview.TabIndex = 9;
            this.btnCodeGenPreview.Text = "预览生成";
            this.btnCodeGenPreview.Click += new System.EventHandler(this.btnCodeGenPreview_Click);
            // 
            // txtPrefix
            // 
            this.txtPrefix.EditValue = "";
            this.txtPrefix.Location = new System.Drawing.Point(431, 103);
            this.txtPrefix.Margin = new System.Windows.Forms.Padding(5);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(200, 28);
            this.txtPrefix.TabIndex = 4;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(307, 106);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(114, 22);
            this.labelControl9.TabIndex = 19;
            this.labelControl9.Text = "自己的类前缀:";
            // 
            // txtEntityPluralName
            // 
            this.txtEntityPluralName.EditValue = "";
            this.txtEntityPluralName.Location = new System.Drawing.Point(791, 65);
            this.txtEntityPluralName.Margin = new System.Windows.Forms.Padding(5);
            this.txtEntityPluralName.Name = "txtEntityPluralName";
            this.txtEntityPluralName.Size = new System.Drawing.Size(203, 28);
            this.txtEntityPluralName.TabIndex = 6;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(664, 68);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(117, 22);
            this.labelControl8.TabIndex = 17;
            this.labelControl8.Text = "实体名(Plural):";
            // 
            // chkPk
            // 
            this.chkPk.Location = new System.Drawing.Point(1134, 27);
            this.chkPk.Name = "chkPk";
            this.chkPk.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkPk.Size = new System.Drawing.Size(167, 28);
            this.chkPk.TabIndex = 8;
            this.chkPk.Visible = false;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(1026, 30);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(100, 22);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Primary Key:";
            this.labelControl7.Visible = false;
            // 
            // cbeBaseClass
            // 
            this.cbeBaseClass.Location = new System.Drawing.Point(791, 102);
            this.cbeBaseClass.Name = "cbeBaseClass";
            this.cbeBaseClass.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeBaseClass.Size = new System.Drawing.Size(203, 28);
            this.cbeBaseClass.TabIndex = 7;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(700, 105);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(83, 22);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "BaseClass:";
            // 
            // txtTableName
            // 
            this.txtTableName.EditValue = "";
            this.txtTableName.Location = new System.Drawing.Point(791, 27);
            this.txtTableName.Margin = new System.Windows.Forms.Padding(5);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Properties.ReadOnly = true;
            this.txtTableName.Size = new System.Drawing.Size(203, 28);
            this.txtTableName.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(737, 30);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(42, 22);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "表名:";
            // 
            // txtProjectName
            // 
            this.txtProjectName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::AbpCodeBulider.DxUI.Properties.Settings.Default, "ProjectName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtProjectName.EditValue = global::AbpCodeBulider.DxUI.Properties.Settings.Default.ProjectName;
            this.txtProjectName.Location = new System.Drawing.Point(111, 65);
            this.txtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(167, 28);
            this.txtProjectName.TabIndex = 1;
            this.txtProjectName.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtProjectName_EditValueChanging);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(23, 68);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(78, 22);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "项目名称:";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::AbpCodeBulider.DxUI.Properties.Settings.Default, "CompanyName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtCompanyName.EditValue = global::AbpCodeBulider.DxUI.Properties.Settings.Default.CompanyName;
            this.txtCompanyName.Location = new System.Drawing.Point(111, 27);
            this.txtCompanyName.Margin = new System.Windows.Forms.Padding(5);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(167, 28);
            this.txtCompanyName.TabIndex = 0;
            this.txtCompanyName.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtCompanyName_EditValueChanging);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(23, 30);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(78, 22);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "公司名称:";
            // 
            // txtEntityName
            // 
            this.txtEntityName.EditValue = "";
            this.txtEntityName.Location = new System.Drawing.Point(431, 65);
            this.txtEntityName.Margin = new System.Windows.Forms.Padding(5);
            this.txtEntityName.Name = "txtEntityName";
            this.txtEntityName.Size = new System.Drawing.Size(200, 28);
            this.txtEntityName.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(361, 68);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 22);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "实体名:";
            // 
            // txtNamespace
            // 
            this.txtNamespace.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", global::AbpCodeBulider.DxUI.Properties.Settings.Default, "Namespace", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtNamespace.EditValue = global::AbpCodeBulider.DxUI.Properties.Settings.Default.Namespace;
            this.txtNamespace.Location = new System.Drawing.Point(431, 27);
            this.txtNamespace.Margin = new System.Windows.Forms.Padding(5);
            this.txtNamespace.Name = "txtNamespace";
            this.txtNamespace.Properties.ReadOnly = true;
            this.txtNamespace.Size = new System.Drawing.Size(200, 28);
            this.txtNamespace.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(343, 30);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 22);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "命名空间:";
            // 
            // chkServer
            // 
            this.chkServer.Location = new System.Drawing.Point(8, 30);
            this.chkServer.Margin = new System.Windows.Forms.Padding(5);
            this.chkServer.Name = "chkServer";
            this.chkServer.Properties.Caption = "生成服务端";
            this.chkServer.Size = new System.Drawing.Size(141, 26);
            this.chkServer.TabIndex = 6;
            // 
            // btnOpenSaveDir
            // 
            this.btnOpenSaveDir.Location = new System.Drawing.Point(1123, 34);
            this.btnOpenSaveDir.Margin = new System.Windows.Forms.Padding(5);
            this.btnOpenSaveDir.Name = "btnOpenSaveDir";
            this.btnOpenSaveDir.Size = new System.Drawing.Size(125, 39);
            this.btnOpenSaveDir.TabIndex = 1;
            this.btnOpenSaveDir.Text = "选择";
            this.btnOpenSaveDir.Click += new System.EventHandler(this.btnOpenSaveDir_Click);
            // 
            // txtSaveCodeDir
            // 
            this.txtSaveCodeDir.EditValue = "C:\\AbpCodeBuilder";
            this.txtSaveCodeDir.Location = new System.Drawing.Point(152, 39);
            this.txtSaveCodeDir.Margin = new System.Windows.Forms.Padding(5);
            this.txtSaveCodeDir.Name = "txtSaveCodeDir";
            this.txtSaveCodeDir.Size = new System.Drawing.Size(940, 28);
            this.txtSaveCodeDir.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 44);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 22);
            this.label4.TabIndex = 1;
            this.label4.Text = "输出目录：";
            // 
            // btnOutput
            // 
            this.btnOutput.Location = new System.Drawing.Point(1258, 35);
            this.btnOutput.Margin = new System.Windows.Forms.Padding(5);
            this.btnOutput.Name = "btnOutput";
            this.btnOutput.Size = new System.Drawing.Size(125, 39);
            this.btnOutput.TabIndex = 2;
            this.btnOutput.Text = "导出";
            this.btnOutput.Click += new System.EventHandler(this.btnOutput_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnOutput);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.txtSaveCodeDir);
            this.groupBox5.Controls.Add(this.btnOpenSaveDir);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(0, 862);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox5.Size = new System.Drawing.Size(1729, 90);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "保存位置";
            // 
            // chkClient
            // 
            this.chkClient.Location = new System.Drawing.Point(8, 66);
            this.chkClient.Margin = new System.Windows.Forms.Padding(5);
            this.chkClient.Name = "chkClient";
            this.chkClient.Properties.Caption = "生成前端(Vue)";
            this.chkClient.Size = new System.Drawing.Size(141, 26);
            this.chkClient.TabIndex = 7;
            // 
            // chkAppService
            // 
            this.chkAppService.Location = new System.Drawing.Point(23, 32);
            this.chkAppService.Margin = new System.Windows.Forms.Padding(5);
            this.chkAppService.Name = "chkAppService";
            this.chkAppService.Properties.Caption = "AppService";
            this.chkAppService.Size = new System.Drawing.Size(170, 26);
            this.chkAppService.TabIndex = 6;
            // 
            // chkAppServiceInterface
            // 
            this.chkAppServiceInterface.Location = new System.Drawing.Point(23, 68);
            this.chkAppServiceInterface.Margin = new System.Windows.Forms.Padding(5);
            this.chkAppServiceInterface.Name = "chkAppServiceInterface";
            this.chkAppServiceInterface.Properties.Caption = "AppServiceInterface";
            this.chkAppServiceInterface.Size = new System.Drawing.Size(187, 26);
            this.chkAppServiceInterface.TabIndex = 7;
            // 
            // chkExportingService
            // 
            this.chkExportingService.Location = new System.Drawing.Point(23, 104);
            this.chkExportingService.Margin = new System.Windows.Forms.Padding(5);
            this.chkExportingService.Name = "chkExportingService";
            this.chkExportingService.Properties.Caption = "ExportingService";
            this.chkExportingService.Size = new System.Drawing.Size(170, 26);
            this.chkExportingService.TabIndex = 8;
            // 
            // chkExportingIntercafe
            // 
            this.chkExportingIntercafe.Location = new System.Drawing.Point(23, 140);
            this.chkExportingIntercafe.Margin = new System.Windows.Forms.Padding(5);
            this.chkExportingIntercafe.Name = "chkExportingIntercafe";
            this.chkExportingIntercafe.Properties.Caption = "ExportingIntercafe";
            this.chkExportingIntercafe.Size = new System.Drawing.Size(187, 26);
            this.chkExportingIntercafe.TabIndex = 9;
            // 
            // chkCreateOrEditInput
            // 
            this.chkCreateOrEditInput.Location = new System.Drawing.Point(237, 32);
            this.chkCreateOrEditInput.Margin = new System.Windows.Forms.Padding(5);
            this.chkCreateOrEditInput.Name = "chkCreateOrEditInput";
            this.chkCreateOrEditInput.Properties.Caption = "CreateOrEditInput";
            this.chkCreateOrEditInput.Size = new System.Drawing.Size(170, 26);
            this.chkCreateOrEditInput.TabIndex = 10;
            // 
            // chkEntityDto
            // 
            this.chkEntityDto.Location = new System.Drawing.Point(237, 68);
            this.chkEntityDto.Margin = new System.Windows.Forms.Padding(5);
            this.chkEntityDto.Name = "chkEntityDto";
            this.chkEntityDto.Properties.Caption = "EntityDto";
            this.chkEntityDto.Size = new System.Drawing.Size(170, 26);
            this.chkEntityDto.TabIndex = 11;
            // 
            // chkGetAllForExcelInput
            // 
            this.chkGetAllForExcelInput.Location = new System.Drawing.Point(237, 104);
            this.chkGetAllForExcelInput.Margin = new System.Windows.Forms.Padding(5);
            this.chkGetAllForExcelInput.Name = "chkGetAllForExcelInput";
            this.chkGetAllForExcelInput.Properties.Caption = "GetAllForExcelInput";
            this.chkGetAllForExcelInput.Size = new System.Drawing.Size(201, 26);
            this.chkGetAllForExcelInput.TabIndex = 12;
            // 
            // chkGetAllInput
            // 
            this.chkGetAllInput.Location = new System.Drawing.Point(237, 140);
            this.chkGetAllInput.Margin = new System.Windows.Forms.Padding(5);
            this.chkGetAllInput.Name = "chkGetAllInput";
            this.chkGetAllInput.Properties.Caption = "GetAllInput";
            this.chkGetAllInput.Size = new System.Drawing.Size(201, 26);
            this.chkGetAllInput.TabIndex = 13;
            // 
            // chkGetForEditOutput
            // 
            this.chkGetForEditOutput.Location = new System.Drawing.Point(447, 32);
            this.chkGetForEditOutput.Margin = new System.Windows.Forms.Padding(5);
            this.chkGetForEditOutput.Name = "chkGetForEditOutput";
            this.chkGetForEditOutput.Properties.Caption = "GetForEditOutput";
            this.chkGetForEditOutput.Size = new System.Drawing.Size(170, 26);
            this.chkGetForEditOutput.TabIndex = 14;
            // 
            // chkDapperMapper
            // 
            this.chkDapperMapper.Location = new System.Drawing.Point(447, 104);
            this.chkDapperMapper.Margin = new System.Windows.Forms.Padding(5);
            this.chkDapperMapper.Name = "chkDapperMapper";
            this.chkDapperMapper.Properties.Caption = "DapperMapper";
            this.chkDapperMapper.Size = new System.Drawing.Size(170, 26);
            this.chkDapperMapper.TabIndex = 15;
            // 
            // chkAuthorizationProvider
            // 
            this.chkAuthorizationProvider.Location = new System.Drawing.Point(447, 140);
            this.chkAuthorizationProvider.Margin = new System.Windows.Forms.Padding(5);
            this.chkAuthorizationProvider.Name = "chkAuthorizationProvider";
            this.chkAuthorizationProvider.Properties.Caption = "Authorization Provider";
            this.chkAuthorizationProvider.Size = new System.Drawing.Size(203, 26);
            this.chkAuthorizationProvider.TabIndex = 16;
            // 
            // chkBatchDeleteInput
            // 
            this.chkBatchDeleteInput.Location = new System.Drawing.Point(447, 68);
            this.chkBatchDeleteInput.Margin = new System.Windows.Forms.Padding(5);
            this.chkBatchDeleteInput.Name = "chkBatchDeleteInput";
            this.chkBatchDeleteInput.Properties.Caption = "BatchDeleteInput";
            this.chkBatchDeleteInput.Size = new System.Drawing.Size(170, 26);
            this.chkBatchDeleteInput.TabIndex = 17;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.chkAdminPermission);
            this.groupBox1.Controls.Add(this.chkAllDto);
            this.groupBox1.Controls.Add(this.chkBatchDeleteInput);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.chkAuthorizationProvider);
            this.groupBox1.Controls.Add(this.chkDapperMapper);
            this.groupBox1.Controls.Add(this.chkGetForEditOutput);
            this.groupBox1.Controls.Add(this.chkGetAllInput);
            this.groupBox1.Controls.Add(this.chkGetAllForExcelInput);
            this.groupBox1.Controls.Add(this.chkEntityDto);
            this.groupBox1.Controls.Add(this.chkCreateOrEditInput);
            this.groupBox1.Controls.Add(this.chkExportingIntercafe);
            this.groupBox1.Controls.Add(this.chkExportingService);
            this.groupBox1.Controls.Add(this.chkAppServiceInterface);
            this.groupBox1.Controls.Add(this.chkAppService);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 525);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox1.Size = new System.Drawing.Size(1729, 180);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "生成列表";
            // 
            // chkAllDto
            // 
            this.chkAllDto.Location = new System.Drawing.Point(660, 140);
            this.chkAllDto.Margin = new System.Windows.Forms.Padding(5);
            this.chkAllDto.Name = "chkAllDto";
            this.chkAllDto.Properties.Caption = "全选";
            this.chkAllDto.Size = new System.Drawing.Size(81, 26);
            this.chkAllDto.TabIndex = 18;
            this.chkAllDto.EditValueChanged += new System.EventHandler(this.chkAllDto_EditValueChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkLocalization);
            this.groupBox4.Controls.Add(this.chkServer);
            this.groupBox4.Controls.Add(this.chkExcelExport);
            this.groupBox4.Controls.Add(this.chkClient);
            this.groupBox4.Location = new System.Drawing.Point(924, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(288, 147);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "User Interface";
            // 
            // chkLocalization
            // 
            this.chkLocalization.Location = new System.Drawing.Point(169, 30);
            this.chkLocalization.Margin = new System.Windows.Forms.Padding(5);
            this.chkLocalization.Name = "chkLocalization";
            this.chkLocalization.Properties.Caption = "生成多语言";
            this.chkLocalization.Size = new System.Drawing.Size(111, 26);
            this.chkLocalization.TabIndex = 9;
            // 
            // chkExcelExport
            // 
            this.chkExcelExport.Location = new System.Drawing.Point(8, 102);
            this.chkExcelExport.Margin = new System.Windows.Forms.Padding(5);
            this.chkExcelExport.Name = "chkExcelExport";
            this.chkExcelExport.Properties.Caption = "创建导出Excel";
            this.chkExcelExport.Size = new System.Drawing.Size(141, 26);
            this.chkExcelExport.TabIndex = 8;
            // 
            // FrmCode
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1729, 1258);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.gcList);
            this.LookAndFeel.SkinName = "Office 2010 Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "FrmCode";
            this.Text = "代码生成";
            this.Load += new System.EventHandler(this.FrmCode_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcList)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdminPermission.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntityPluralName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeBaseClass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTableName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntityName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamespace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkServer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaveCodeDir.Properties)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkClient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAppService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAppServiceInterface.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportingService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportingIntercafe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateOrEditInput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEntityDto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetAllForExcelInput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetAllInput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetForEditOutput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDapperMapper.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationProvider.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBatchDeleteInput.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkAllDto.Properties)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkLocalization.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcelExport.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chk;
        private DevExpress.XtraGrid.Views.Grid.GridView gvList;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.GridControl gcList;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.CheckEdit chkAdminPermission;
        private DevExpress.XtraEditors.SimpleButton btnCodeGenPreview;
        private DevExpress.XtraEditors.TextEdit txtPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtEntityPluralName;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ComboBoxEdit chkPk;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit cbeBaseClass;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtTableName;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtProjectName;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtCompanyName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtEntityName;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtNamespace;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit chkServer;
        private DevExpress.XtraEditors.SimpleButton btnOpenSaveDir;
        private DevExpress.XtraEditors.TextEdit txtSaveCodeDir;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.SimpleButton btnOutput;
        private System.Windows.Forms.GroupBox groupBox5;
        private DevExpress.XtraEditors.CheckEdit chkClient;
        private DevExpress.XtraEditors.CheckEdit chkAppService;
        private DevExpress.XtraEditors.CheckEdit chkAppServiceInterface;
        private DevExpress.XtraEditors.CheckEdit chkExportingService;
        private DevExpress.XtraEditors.CheckEdit chkExportingIntercafe;
        private DevExpress.XtraEditors.CheckEdit chkCreateOrEditInput;
        private DevExpress.XtraEditors.CheckEdit chkEntityDto;
        private DevExpress.XtraEditors.CheckEdit chkGetAllForExcelInput;
        private DevExpress.XtraEditors.CheckEdit chkGetAllInput;
        private DevExpress.XtraEditors.CheckEdit chkGetForEditOutput;
        private DevExpress.XtraEditors.CheckEdit chkDapperMapper;
        private DevExpress.XtraEditors.CheckEdit chkAuthorizationProvider;
        private DevExpress.XtraEditors.CheckEdit chkBatchDeleteInput;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.CheckEdit chkExcelExport;
        private System.Windows.Forms.GroupBox groupBox4;
        private DevExpress.XtraEditors.CheckEdit chkAllDto;
        private DevExpress.XtraEditors.CheckEdit chkLocalization;
    }
}