﻿{{Enum_Using_Looped_Template_Here}}
using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace {{Namespace_Here}}.{{Namespace_Relative_Full_Here}}.Dto
{
    [AutoMap(typeof({{Entity_Name_Plural_Here}}.{{Entity_Name_Here}}))]
    public class {{Entity_Name_Here}}Dto : EntityDto{{Primary_Key_Inside_Tag_Here}}
    {
{{Property_Looped_Template_Here}}
    }
}