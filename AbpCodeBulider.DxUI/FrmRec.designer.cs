﻿namespace AbpCodeBulider.DxUI
{
    partial class FrmRec
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rec = new DevExpress.XtraRichEdit.RichEditControl();
            this.SuspendLayout();
            // 
            // rec
            // 
            this.rec.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.rec.Appearance.Text.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rec.Appearance.Text.Options.UseFont = true;
            this.rec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rec.Location = new System.Drawing.Point(0, 0);
            this.rec.Name = "rec";
            this.rec.Options.AutoCorrect.DetectUrls = false;
            this.rec.Options.AutoCorrect.ReplaceTextAsYouType = false;
            this.rec.Options.Behavior.PasteLineBreakSubstitution = DevExpress.XtraRichEdit.LineBreakSubstitute.Paragraph;
            this.rec.Options.DocumentCapabilities.Bookmarks = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.CharacterStyle = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.HeadersFooters = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Hyperlinks = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.InlinePictures = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Numbering.Bulleted = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Numbering.MultiLevel = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Numbering.Simple = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.ParagraphFormatting = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Paragraphs = DevExpress.XtraRichEdit.DocumentCapability.Enabled;
            this.rec.Options.DocumentCapabilities.ParagraphStyle = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Sections = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Tables = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.TableStyle = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this.rec.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.rec.Options.MailMerge.KeepLastParagraph = false;
            this.rec.Size = new System.Drawing.Size(942, 527);
            this.rec.TabIndex = 4;
            this.rec.Views.DraftView.AllowDisplayLineNumbers = true;
            this.rec.Views.DraftView.Padding = new System.Windows.Forms.Padding(80, 4, 0, 0);
            this.rec.Views.SimpleView.Padding = new System.Windows.Forms.Padding(50, 4, 4, 0);
            // 
            // FrmRec
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 527);
            this.Controls.Add(this.rec);
            this.LookAndFeel.SkinName = "Office 2010 Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "FrmRec";
            this.Text = "FrmRec";
            this.Load += new System.EventHandler(this.FrmRec_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraRichEdit.RichEditControl rec;

    }
}