﻿using System;
using System.ComponentModel;
using System.Data;

namespace AbpCodeBulider.DxUI
{
    internal class FrmBase : DevExpress.XtraEditors.XtraForm
    {
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FrmBase
            // 
            this.ClientSize = new System.Drawing.Size(833, 645);this.Name = "FrmBase";
            this.Load += new System.EventHandler(this.FrmBase_Load);
            this.ResumeLayout(false);

        }

        #region 变量和属性

        [Browsable(false)]
        public string strName { get; set; }

        protected FrmMain _FrmMain;
        [Browsable(false)]
        public FrmMain FrmMain
        {
            get { return _FrmMain; }
            set { _FrmMain = value; }
        }

        [Browsable(false)]
        internal protected Action LastAction
        {
            get { return _FrmMain == null ? null : _FrmMain.lastAction; }
            set { _FrmMain.lastAction = value; }
        }
        /// <summary>
        /// 数据库名
        /// </summary>
        [Browsable(false)]
        public string DBName
        {
            get { return _FrmMain == null ? null : _FrmMain.DBName; }
        }
        /// <summary>
        /// 表名
        /// </summary>
        [Browsable(false)]
        public string DataTableName
        {
            get { return _FrmMain == null ? null : _FrmMain.DataTableName; }
        }
        [Browsable(false)]
        public string DataTableId
        {
            get { return _FrmMain == null ? null : _FrmMain.DataTableId; }
        }
        [Browsable(false)]
        public string Xtype
        {
            get { return _FrmMain == null ? null : _FrmMain.Xtype; }
        }

        [Browsable(false)]
        public DataTable DtNames
        {
            get { return _FrmMain == null ? null : _FrmMain.DtNames; }
            set { _FrmMain.DtNames = value; }
        }
        [Browsable(false)]
        public DataTable DtDbName
        {
            get { return _FrmMain == null?null:_FrmMain.DtDbName; }
            set { _FrmMain.DtDbName = value; }
        }
        #endregion

        public FrmBase()
        {
            InitializeComponent();
        }


        #region 事件
        #region 生成GridView的行号
        protected void gvList_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0)
                {
                    e.Info.DisplayText = (e.RowHandle + 1).ToString();
                }
                else if (e.RowHandle < 0 && e.RowHandle > -1000)
                {
                    e.Info.Appearance.BackColor = System.Drawing.Color.AntiqueWhite;
                    e.Info.DisplayText = "G" + e.RowHandle.ToString();
                }
            }
        }
        #endregion

        private void FrmBase_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.LookAndFeel.SetSkinStyle(FrmMain.strSkin);
            }
        }
        #endregion
    }
}
