﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbpCodeBuilder.CodeHelper
{
    public class ServerGenerator
    {
        #region Server

        #region 生成接口信息

        /// <summary> 生成接口信息
        /// </summary>
        public static string GenAppServiceInterfaceClass(GenerateMetaInfo generateMetaInfo, bool isExcelExport = false)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Server\AppServiceInterfaceClass\MainTemplate.txt";

            var templateContent = CodeCommon.Read(dir);

            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{Primary_Key_Inside_Tag_Here}}", generateMetaInfo.PrimaryKeyType);


            if (isExcelExport)
            {
                string temp =
                    $"Task<FileDto> Get{generateMetaInfo.EntityNamePlural}ToExcel(GetAll{generateMetaInfo.EntityNamePlural}ForExcelInput input);";
                templateContent = templateContent.Replace("{{Get_Excel_Method_Here}}", temp);
            }
            else
            {
                templateContent = templateContent.Replace("{{Get_Excel_Method_Here}}", "");
            }

            return templateContent;
        }

        #endregion

        #region 生成接口实现类信息

        /// <summary>
        /// 生成接口实现类信息
        /// </summary>
        public static string GenAppServiceClass(GenerateMetaInfo generateMetaInfo, bool isExcelExport = false)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Server\AppServiceClass\MainTemplate.txt";

            var templateContent = CodeCommon.Read(dir);

            var primaryKeyWithCommaHere = "";
            if (generateMetaInfo.PrimaryKeyType != "int")
            {
                primaryKeyWithCommaHere = "," + generateMetaInfo.PrimaryKeyType;
            }

            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{Primary_Key_Inside_Tag_Here}}", generateMetaInfo.PrimaryKeyType)
                .Replace("{{entity_Name_Here}}", CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName))
                .Replace("{{Permission_Name_Here}}", generateMetaInfo.PermissionName)
                .Replace("{{Project_Name_Here}}", generateMetaInfo.ProjectName)
                .Replace("{{Primary_Key_Name}}", generateMetaInfo.PrimaryKeyName)
                .Replace("{{Table_Name_Here}}", generateMetaInfo.TableName)
                .Replace("{{Primary_Key_With_Comma_Here}}", primaryKeyWithCommaHere);

            if (isExcelExport)
            {
                string excelDeclarationHere =
                    $"private readonly I{generateMetaInfo.EntityName}ExcelExporter _{CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName)}ExcelExporter;";

                string excelConstructorParameterHere =
                    $", I{generateMetaInfo.EntityName}ExcelExporter {CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName)}ExcelExporter";

                string excelConstructorInitHere =
                    $"_{generateMetaInfo.EntityName}ExcelExporter = {CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName)}ExcelExporter";

                StringBuilder getExcelMethodHere = new StringBuilder();
                getExcelMethodHere.AppendLine(
                    $"public async Task<FileDto> Get{generateMetaInfo.EntityNamePlural}ToExcel(GetAll{generateMetaInfo.EntityNamePlural}ForExcelInput input");
                getExcelMethodHere.AppendLine("\t\t{");
                getExcelMethodHere.AppendLine($"\t\t\tvar query = Create{generateMetaInfo.EntityName}Query(input);");
                getExcelMethodHere.AppendLine("\t\t\tvar results = await query");
                getExcelMethodHere.AppendLine("\t\t\t\t.AsNoTracking()");
                getExcelMethodHere.AppendLine("\t\t\t\t.OrderBy(input.Sorting ?? \"CreationTime desc\")");
                getExcelMethodHere.AppendLine("\t\t\t\t.ToListAsync();");
                getExcelMethodHere.AppendLine($"\t\t\tvar {CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName)}Dtos =  ObjectMapper.Map<List<{generateMetaInfo.EntityName}Dto>>(results);");

                getExcelMethodHere.AppendLine($"\t\t\treturn _{CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName)}ExcelExporter.ExportToFile({CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName)}ListDtos);");
                getExcelMethodHere.AppendLine("\t\t}");

                templateContent = templateContent.Replace("{{Excel_Declaration_Here}}", excelDeclarationHere);
                templateContent = templateContent.Replace("{{Excel_Constructor_Parameter_Here}}", excelConstructorParameterHere);
                templateContent = templateContent.Replace("{{Excel_Constructor_Init_Here}}", excelConstructorInitHere);
                templateContent = templateContent.Replace("{{Get_Excel_Method_Here}}", getExcelMethodHere.ToString());
            }
            else
            {
                templateContent = templateContent.Replace("{{Excel_Declaration_Here}}", "");
                templateContent = templateContent.Replace("{{Excel_Constructor_Parameter_Here}}", "");
                templateContent = templateContent.Replace("{{Excel_Constructor_Init_Here}}", "");
                templateContent = templateContent.Replace("{{Get_Excel_Method_Here}}", "");
            }

            return templateContent;
        }

        #endregion

        #region 生成Exporting接口信息
        /// <summary>
        /// 生成Exporting接口信息
        /// </summary>
        public static string GenExportingIntercafeClass(GenerateMetaInfo generateMetaInfo)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Server\ExportingIntercafeClass\MainTemplate.txt";

            var templateContent = CodeCommon.Read(dir);

            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{entity_Name_Here}}", CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName));

            return templateContent;
        }
        #endregion

        #region 生成ExportingClass
        /// <summary>
        /// 生成ExportingClass
        /// </summary>
        public static string GenExportingClass(GenerateMetaInfo generateMetaInfo, List<ColumnInfo> colInfo)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Server\ExportingClass\MainTemplate.txt";

            var templateContent = CodeCommon.Read(dir);


            StringBuilder excel_Header = new StringBuilder();
            StringBuilder excel_Objects = new StringBuilder();

            for (int i = 0; i < colInfo.Count; i++)
            {
                var item = colInfo[i];
                if (item.IsIdentity || item.ColumnName.ToLower() == "id")
                    continue;

                if (i == 0)
                {
                    excel_Header.AppendLine($"				    L(\"{colInfo[i].ColumnName}\"),");
                    excel_Objects.AppendLine($"				    _ => _.{colInfo[i].ColumnName},");
                }
                else
                {
                    var comma = string.Empty;
                    if (i + 1 < colInfo.Count)
                    {
                        comma = ",";
                    }

                    excel_Header.AppendLine($"				    L(\"{colInfo[i].ColumnName}\")" + comma);
                    excel_Objects.AppendLine($"				    _ => _.{colInfo[i].ColumnName}" + comma);
                }
            }

            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural)
                .Replace("{{entity_Name_Here}}", CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName))
                .Replace("{{Permission_Name_Here}}", generateMetaInfo.PermissionName)
                .Replace("{{Excel_Header}}", excel_Header.ToString())
                .Replace("{{Excel_Objects}}", excel_Objects.ToString());

            return templateContent;
        }
        #endregion

        #region 生成GetAllForExcelInputClass

        /// <summary>
        /// 生成GetAllForExcelInputClass
        /// </summary>
        public static string GenGetAllForExcelInputClass(GenerateMetaInfo generateMetaInfo)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Server\Dto\GetAllForExcelInputClass\MainTemplate.txt";
            var templateContent = CodeCommon.Read(dir);

            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural);

            return templateContent;
        }

        #endregion

        #region 生成GetAllInputClass

        /// <summary>
        /// 生成GetAllInputClass
        /// </summary>
        public static string GenGetAllInputClass(GenerateMetaInfo generateMetaInfo)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Server\Dto\GetAllInputClass\MainTemplate.txt";
            var templateContent = CodeCommon.Read(dir);

            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural);

            return templateContent;
        }

        #endregion

        #region 生成GetForEditOutputClass

        /// <summary>
        /// 生成GetForEditOutputClass
        /// </summary>
        public static string GenGetForEditOutputClass(GenerateMetaInfo generateMetaInfo)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Server\Dto\GetForEditOutputClass\MainTemplate.txt";
            var templateContent = CodeCommon.Read(dir);

            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName);

            return templateContent;

        }

        #endregion

        #region 生成EntityDtoClass

        /// <summary>
        /// 生成EntityDtoClass
        /// </summary>
        public static string GenEntityDtoClass(GenerateMetaInfo generateMetaInfo, List<ColumnInfo> colInfo)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Server\Dto\EntityDtoClass\MainTemplate.txt";
            var templateContent = CodeCommon.Read(dir);

            StringBuilder sb = new StringBuilder();

            foreach (var item in colInfo)
            {
                if (item.IsPrimaryKey)
                    continue;
                sb.AppendLine("		public " + item.ColumnType + " " + item.ColumnName + " { get; set; }");
                sb.AppendLine("		");
            }
            var propertyLoopedTemplate = sb.ToString();
            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural)
                .Replace("{{Primary_Key_Inside_Tag_Here}}", $"<{generateMetaInfo.PrimaryKeyType}>")
                .Replace("{{Property_Looped_Template_Here}}", propertyLoopedTemplate)
                .Replace("{{Enum_Using_Looped_Template_Here}}", "");

            return templateContent;
        }

        #endregion

        #region 生成CreateOrEditDtoClass

        /// <summary>
        /// 生成CreateOrEditDtoClass
        /// </summary>
        public static string GenCreateOrEditDtoClass(GenerateMetaInfo generateMetaInfo, List<ColumnInfo> colInfo)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Server\Dto\CreateOrEditDtoClass\MainTemplate.txt";
            var templateContent = CodeCommon.Read(dir);

            StringBuilder sb = new StringBuilder();

            foreach (var item in colInfo)
            {
                if (item.IsPrimaryKey || item.IsIdentity)
                    continue;
                if (!item.Nullable && item.ColumnType == "string")
                {
                    sb.AppendLine("		[Required(ErrorMessage = \"ThisFieldIsRequired\")]");
                }

                if (item.Nullable && item.ColumnType != "string")
                {
                    sb.AppendLine($"		public {item.ColumnType}? {item.ColumnName} {{ get; set; }}");
                }
                else
                {
                    sb.AppendLine($"		public {item.ColumnType} {item.ColumnName} {{ get; set; }}");
                }

                sb.AppendLine("     ");
            }
            var propertyLoopedTemplate = sb.ToString();
            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural)
                .Replace("{{Base_Class_Here}}", generateMetaInfo.BaseClass)
                .Replace("{{Primary_Key_Inside_Tag_Here}}", $"<{generateMetaInfo.PrimaryKeyType}?>")
                .Replace("{{Property_Looped_Template_Here}}", propertyLoopedTemplate);

            return templateContent;
        }

        #endregion

        #region 生成BatchDeleteInputClass

        /// <summary>
        /// 生成BatchDeleteInputClass
        /// </summary>
        public static string GenBatchDeleteInputClass(GenerateMetaInfo generateMetaInfo)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory +
                      @"FileTemplates\Server\Dto\BatchDeleteInputClass\MainTemplate.txt";
            var templateContent = CodeCommon.Read(dir);

            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{Primary_Key_Inside_Tag_Here}}", generateMetaInfo.PrimaryKeyType)
                .Replace("{{Primary_Key_Name}}", generateMetaInfo.PrimaryKeyName)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural);

            return templateContent;
        }

        #endregion

        #region 生成DapperMapperClass

        /// <summary>
        /// 生成DapperMapperClass
        /// </summary>
        public static string GenDapperMapperClass(GenerateMetaInfo generateMetaInfo)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory +
                      @"FileTemplates\Server\DapperMapperClass\MainTemplate.txt";
            var templateContent = CodeCommon.Read(dir);

            templateContent = templateContent.Replace("{{Namespace_Here}}", generateMetaInfo.Namespace)
                .Replace("{{Namespace_Relative_Full_Here}}", generateMetaInfo.NamespaceRelativeFull)
                .Replace("{{Table_Name_Here}}", generateMetaInfo.TableName)
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural);

            return templateContent;
        }

        #endregion

        #region 生成GenAdminPermissions

        /// <summary>
        /// 生成GenAdminPermissions
        /// </summary>
        /// <param name="entityPluralName"></param>
        public static string GenAdminPermissions(string entityPluralName)
        {
            StringBuilder sbAppPermissionsHere = new StringBuilder();
            sbAppPermissionsHere.AppendLine($"#region {entityPluralName}");
            sbAppPermissionsHere.AppendLine();

            sbAppPermissionsHere.AppendLine(
                $"public const string Pages_Administration_{entityPluralName} = \"Pages.Administration.{entityPluralName}\";");
            sbAppPermissionsHere.AppendLine(
                $"public const string Pages_Administration_{entityPluralName}_Create = \"Pages.Administration.{entityPluralName}.Create\";");
            sbAppPermissionsHere.AppendLine(
                $"public const string Pages_Administration_{entityPluralName}_Edit = \"Pages.Administration.{entityPluralName}.Edit\";");
            sbAppPermissionsHere.AppendLine(
                $"public const string Pages_Administration_{entityPluralName}_Delete = \"Pages.Administration.{entityPluralName}.Delete\";");
            sbAppPermissionsHere.AppendLine();
            sbAppPermissionsHere.AppendLine(" #endregion");
            sbAppPermissionsHere.AppendLine("                         ");
            sbAppPermissionsHere.AppendLine(" //{{AppPermissions_Here}}");

            string templateContent = sbAppPermissionsHere.ToString();

            return templateContent;
        }

        #endregion

        #region 生成GenAppPermissions

        /// <summary>
        /// 生成GenAppPermissions
        /// </summary>
        /// <param name="entityPluralName"></param>
        public static string GenAppPermissions(string entityPluralName)
        {
            StringBuilder sbAppPermissions = new StringBuilder();
            sbAppPermissions.AppendLine($"#region {entityPluralName}");
            sbAppPermissions.AppendLine();

            sbAppPermissions.AppendLine(
                $"public const string Pages_{entityPluralName} = \"Pages.{entityPluralName}\";");
            sbAppPermissions.AppendLine(
                $"public const string Pages_{entityPluralName}_Create = \"Pages.{entityPluralName}.Create\";");
            sbAppPermissions.AppendLine(
                $"public const string Pages_{entityPluralName}_Edit = \"Pages.{entityPluralName}.Edit\";");
            sbAppPermissions.AppendLine(
                $"public const string Pages_{entityPluralName}_Delete = \"Pages.{entityPluralName}.Delete\";");
            sbAppPermissions.AppendLine();
            sbAppPermissions.AppendLine(" #endregion");
            sbAppPermissions.AppendLine("                         ");
            sbAppPermissions.AppendLine(" //{{AppPermissions_Here}}");

            string templateContent = sbAppPermissions.ToString();

            return templateContent;

        }

        #endregion

        #region 生成GenAdminAuthorizationProvider

        /// <summary>
        /// 生成GenAdminAuthorizationProvider
        /// </summary>
        public static string GenAdminAuthorizationProvider(string entityName, string entityPluralName)
        {
            StringBuilder sbAppAuthorizationProvider = new StringBuilder();
            sbAppAuthorizationProvider.AppendLine($"#region {entityPluralName}");
            sbAppAuthorizationProvider.AppendLine();
            sbAppAuthorizationProvider.AppendLine(
                $" var {entityPluralName} = context.CreateChildPermission(AppPermissions.Pages_Administration_{entityPluralName}, L(\"{entityPluralName}\"));");
            sbAppAuthorizationProvider.AppendLine(
                $"{entityPluralName}.CreateChildPermission(AppPermissions.Pages_Administration_{entityPluralName}_Create, L(\"CreatingNew{entityName}\"));");
            sbAppAuthorizationProvider.AppendLine(
                $"{entityPluralName}.CreateChildPermission(AppPermissions.Pages_Administration_{entityPluralName}_Edit, L(\"Editing{entityName}\"));");
            sbAppAuthorizationProvider.AppendLine(
                $"{entityPluralName}.CreateChildPermission(AppPermissions.Pages_Administration_{entityPluralName}_Delete, L(\"Deleting{entityName}\"));");
            sbAppAuthorizationProvider.AppendLine();
            sbAppAuthorizationProvider.AppendLine(" #endregion");
            sbAppAuthorizationProvider.AppendLine("                         ");
            sbAppAuthorizationProvider.AppendLine(" //{{AppAuthorizationProvider_Here}}");

            string templateContent = sbAppAuthorizationProvider.ToString();

            return templateContent;
        }

        #endregion

        #region 生成GenAuthorizationProvider

        /// <summary>
        /// 生成GenAuthorizationProvider
        /// </summary>
        public static string GenAppAuthorizationProvider(string entityName, string entityPluralName)
        {
            StringBuilder sbAppAuthorizationProvider = new StringBuilder();
            sbAppAuthorizationProvider.AppendLine($"#region {entityPluralName}");
            sbAppAuthorizationProvider.AppendLine();
            sbAppAuthorizationProvider.AppendLine(
                $" var {entityPluralName} = context.CreateChildPermission(AppPermissions.Pages_{entityPluralName}, L(\"{entityPluralName}\"));");
            sbAppAuthorizationProvider.AppendLine(
                $"{entityPluralName}.CreateChildPermission(AppPermissions.Pages_{entityPluralName}_Create, L(\"CreatingNew{entityName}\"));");
            sbAppAuthorizationProvider.AppendLine(
                $"{entityPluralName}.CreateChildPermission(AppPermissions.Pages_{entityPluralName}_Edit, L(\"Editing{entityName}\"));");
            sbAppAuthorizationProvider.AppendLine(
                $"{entityPluralName}.CreateChildPermission(AppPermissions.Pages_{entityPluralName}_Delete, L(\"Deleting{entityName}\"));");
            sbAppAuthorizationProvider.AppendLine();
            sbAppAuthorizationProvider.AppendLine(" #endregion");
            sbAppAuthorizationProvider.AppendLine("                         ");

            string templateContent = sbAppAuthorizationProvider.ToString();

            return templateContent;
        }

        #endregion

        #region 生成本地化语言 xml 文档

        /// <summary>
        /// 生成本地化语言 xml 文档
        /// </summary>
        /// <returns></returns>
        public static string GenLocalization(GenerateMetaInfo generateMetaInfo, List<ColumnInfo> colInfo)
        {
            StringBuilder localizationDictionaryHere = new StringBuilder();
            localizationDictionaryHere.AppendLine($"<text name=\"{generateMetaInfo.EntityNamePlural}\">{generateMetaInfo.EntityNamePlural}</text>");
            localizationDictionaryHere.AppendLine($"<text name=\"CreatingNew{generateMetaInfo.EntityName}\">创建{generateMetaInfo.EntityName}</text>");
            localizationDictionaryHere.AppendLine($"<text name=\"Editing{generateMetaInfo.EntityName}\">编辑{generateMetaInfo.EntityName}</text>");
            localizationDictionaryHere.AppendLine($"<text name=\"Deleting{generateMetaInfo.EntityName}\">删除{generateMetaInfo.EntityName}</text>");
            localizationDictionaryHere.AppendLine($"<text name=\"{generateMetaInfo.EntityName}DeleteWarningMessage\">您确定要删除 {generateMetaInfo.EntityName}吗?</text>");

            foreach (var item in colInfo)
            {
                if (item.ColumnName.ToLower() == "id")
                    continue;
                localizationDictionaryHere.AppendLine($"<text name=\"{item.ColumnName}\">{item.Description}</text>");
            }

            return localizationDictionaryHere.ToString();
        } 

        #endregion

        #endregion
    }
}