﻿namespace AbpCodeBuilder.CodeHelper
{
    public class GenerateMetaInfo
    {
        public string CompanyName { get; set; }

        public string ProjectName { get; set; }

        public string Namespace => $"{CompanyName}.{ProjectName}";

        public string NamespaceRelativeFull { get; set; }

        public string TableName { get; set; }

        public string EntityName { get; set; }

        public string EntityNamePlural { get; set; }

        public string BaseClass { get; set; }

        public string PrimaryKeyType { get; set; }

        public string PrimaryKeyName { get; set; }

        public string PermissionName { get; set; }

        public bool PagePermissionHost { get; set; }

        public bool PagePermissionTenant { get; set; }
    }
}