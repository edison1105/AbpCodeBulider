﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbpCodeBuilder.CodeHelper
{
    public class ClientGenerator
    {
        #region 生成VueModule
        /// <summary>
        /// 生成VueModule
        /// </summary>
        public static string GenModule(GenerateMetaInfo generateMetaInfo, List<ColumnInfo> colInfo, bool isExportExcel = false)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Client\Vue\Module\MainTemplate.txt";
            var templateContent = CodeCommon.Read(dir);

            templateContent = templateContent
                .Replace("{{entity_Name_Here}}", CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName))
                .Replace("{{entity_Name_Plural_Here}}",
                    CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityNamePlural));

            if (isExportExcel)
            {
                StringBuilder vueModuleExcelMethod = new StringBuilder();
                vueModuleExcelMethod.AppendLine($"        async get{generateMetaInfo.EntityNamePlural}ToExcel({{");
                vueModuleExcelMethod.AppendLine($"            state");
                vueModuleExcelMethod.AppendLine($"        }}, payload) {{");
                vueModuleExcelMethod.AppendLine($"            let rep = await Util.ajax.get('/api/services/app/{CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName)}/Get{generateMetaInfo.EntityNamePlural}ToExcel', payload.data)");
                vueModuleExcelMethod.AppendLine($"            return rep.data.result");
                vueModuleExcelMethod.AppendLine($"        }}");

                templateContent = templateContent.Replace("{{Get_Excel_Method_Here}}", vueModuleExcelMethod.ToString());
            }
            else
            {
                templateContent = templateContent.Replace("{{Get_Excel_Method_Here}}", "");
            }

            return templateContent;
        }
        #endregion

        #region 生成VueView
        /// <summary>
        /// 生成VueView
        /// </summary>
        public static string GenView(GenerateMetaInfo generateMetaInfo, List<ColumnInfo> colInfo, bool isExportExcel = false)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + @"FileTemplates\Client\Vue\View\MainTemplate.txt";
            var templateContent = CodeCommon.Read(dir);


            StringBuilder propertyLoopedTemplate = new StringBuilder();
            StringBuilder propertyRulesTemplateHere = new StringBuilder();
            StringBuilder propertyLoopedEditModalTemplateHere = new StringBuilder();

            foreach (var item in colInfo)
            {
                if (item.IsIdentity || item.ColumnName.ToLower() == "id")
                    continue;
                propertyLoopedTemplate.AppendLine("\t\t\t\t\t{");
                propertyLoopedTemplate.AppendLine($"\t\t\t\t\t\ttitle: this.L('{item.ColumnName}'),");
                propertyLoopedTemplate.AppendLine($"\t\t\t\t\t\tkey: '{CodeCommon.GetFirstToLowerStr(item.ColumnName)}',");
                propertyLoopedTemplate.AppendLine("\t\t\t\t\t\twidth: 180");
                propertyLoopedTemplate.AppendLine("\t\t\t\t\t},");

                if (!item.Nullable && item.ColumnType == "string")
                {
                    propertyRulesTemplateHere.AppendLine($"\t\t\t\t\t{CodeCommon.GetFirstToLowerStr(item.ColumnName)}: [{{");
                    propertyRulesTemplateHere.AppendLine("\t\t\t\t\t\trequired: true,");
                    propertyRulesTemplateHere.AppendLine("\t\t\t\t\t\tmessage: this.L('ThisFieldIsRequired'),");
                    propertyRulesTemplateHere.AppendLine("\t\t\t\t\t\trigger: 'blur',");
                    propertyRulesTemplateHere.AppendLine("\t\t\t\t\t}],");
                }

                if (item.ColumnType == "string")
                {
                    propertyLoopedEditModalTemplateHere.AppendLine($"\t\t\t\t\t<i-form-item :label=\"L('{item.ColumnName}')\" prop='{CodeCommon.GetFirstToLowerStr(item.ColumnName)}'>");
                    propertyLoopedEditModalTemplateHere.AppendLine($"\t\t\t\t\t\t<i-input v-model='edit{{Entity_Name_Here}}.{CodeCommon.GetFirstToLowerStr(item.ColumnName)}'></i-input>");
                    propertyLoopedEditModalTemplateHere.AppendLine("\t\t\t\t\t</i-form-item>");
                }
                else if (item.ColumnType == "int")
                {
                    propertyLoopedEditModalTemplateHere.AppendLine($"\t\t\t\t\t<i-form-item :label=\"L('{item.ColumnName}')\" prop='{CodeCommon.GetFirstToLowerStr(item.ColumnName)}'>");
                    propertyLoopedEditModalTemplateHere.AppendLine($"\t\t\t\t\t\t<i-input-number v-model='edit{generateMetaInfo.EntityName}.{CodeCommon.GetFirstToLowerStr(item.ColumnName)}'></i-input-number>");
                    propertyLoopedEditModalTemplateHere.AppendLine("\t\t\t\t\t</i-form-item>");
                }
            }

            templateContent = templateContent
                .Replace("{{Entity_Name_Plural_Here}}", generateMetaInfo.EntityNamePlural)
                .Replace("{{entity_Name_Plural_Here}}",
                    CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityNamePlural))
                .Replace("{{Permission_Name_Here}}", generateMetaInfo.PermissionName)
                .Replace("{{entity_Name_Plural_Here}}",
                    CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityNamePlural))
                .Replace("{{Entity_Name_Here}}", generateMetaInfo.EntityName)
                .Replace("{{entity_Name_Here}}", CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName))
                .Replace("{{primary_Key_Name}}", CodeCommon.GetFirstToLowerStr(generateMetaInfo.PrimaryKeyName))
                .Replace("{{primary_Key_Inside_Tag_Here}}",
                    CodeCommon.GetFirstToLowerStr(generateMetaInfo.PrimaryKeyType))
                .Replace("{{Property_Looped_Edit_Modal_Template_Here}}", propertyLoopedEditModalTemplateHere.ToString())
                .Replace("{{Property_Looped_Template_Here}}", propertyLoopedTemplate.ToString())
                .Replace("{{Property_Rules_Template_Here}}", propertyRulesTemplateHere.ToString());


            if (isExportExcel)
            {
                StringBuilder vueViewBtnExcel = new StringBuilder();
                vueViewBtnExcel.AppendLine(
                    $"                <i-button type='ghost' icon='ios-download-outline' @click='export{generateMetaInfo.EntityNamePlural}' >{{{{ L('Export') }}}}</i-button>");

                StringBuilder vueViewExcelMethod = new StringBuilder();
                vueViewExcelMethod.AppendLine($"\t\t\tasync export{generateMetaInfo.EntityNamePlural}() {{");
                vueViewExcelMethod.AppendLine("\t\t\t\tthis.loading = true");
                vueViewExcelMethod.AppendLine("\t\t\t\tawait this.$store.dispatch({");
                vueViewExcelMethod.AppendLine($"\t\t\t\t\ttype: '{CodeCommon.GetFirstToLowerStr(generateMetaInfo.EntityName)}/get{generateMetaInfo.EntityNamePlural}ToExcel',");
                vueViewExcelMethod.AppendLine("\t\t\t\t\tdata: {");
                vueViewExcelMethod.AppendLine($"\t\t\t\t\t\t{CodeCommon.GetFirstToLowerStr(generateMetaInfo.PrimaryKeyName)}s: this.select{generateMetaInfo.EntityNamePlural}");
                vueViewExcelMethod.AppendLine("\t\t\t\t\t}");
                vueViewExcelMethod.AppendLine("\t\t\t\t}).then(async (result) => {");
                vueViewExcelMethod.AppendLine("\t\t\t\t\tawait this.$store.dispatch({");
                vueViewExcelMethod.AppendLine("\t\t\t\t\t\ttype: 'file/downloadTempFile',");
                vueViewExcelMethod.AppendLine("\t\t\t\t\t\tdata: result");
                vueViewExcelMethod.AppendLine("\t\t\t\t\t})");
                vueViewExcelMethod.AppendLine("\t\t\t\t\tabp.notify.success(this.L('ExportSuccessfully'))");
                vueViewExcelMethod.AppendLine("\t\t\t\t\tthis.loading = false");
                vueViewExcelMethod.AppendLine("\t\t\t\t}).catch(e => {");
                vueViewExcelMethod.AppendLine("\t\t\t\t\tthis.loading = false");
                vueViewExcelMethod.AppendLine("\t\t\t\t})");
                vueViewExcelMethod.AppendLine("\t\t\t}");

                templateContent = templateContent.Replace("{{Get_Excel_Btn_Here}}", vueViewBtnExcel.ToString());
                templateContent = templateContent.Replace("{{Get_Excel_Method_Here}}", vueViewExcelMethod.ToString());
            }
            else
            {
                templateContent = templateContent.Replace("{{Get_Excel_Btn_Here}}", "");
                templateContent = templateContent.Replace("{{Get_Excel_Method_Here}}", "");
            }


            return templateContent;
        } 
        #endregion
    }
}