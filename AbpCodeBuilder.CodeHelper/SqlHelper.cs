﻿using System.Data;
using System.Data.SqlClient;

namespace AbpCodeBuilder.CodeHelper
{
    public class SqlHelper
    {
        public static string ConnString;//= "Data Source=LEO-PMIS;Initial Catalog=master;User ID=sa;password=P@ssw0rd";
        public static string EBconnString; //= "server=LEO-PMIS;uid=sa;pwd=P@ssw0rd;database=";
        public static string SqliteString;

        #region--------数据库连接select为SQL语句connectionString为连接字符串--------------
        public static DataTable Query(string select)
        {
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(ConnString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter(select, connection);
                adapter.Fill(ds, "temp");
                return ds.Tables["temp"];
            }
        }
        /// <summary>
        /// 连接字符串+数据库名
        /// </summary>
        /// <param name="select"></param>
        /// <param name="databaseName"></param>
        /// <returns></returns>
        public static DataTable Query(string select, string databaseName)
        {
            DataSet ds = new DataSet();
            string connectionString = EBconnString + databaseName;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter(select, connection);
                adapter.Fill(ds, "temp");
                return ds.Tables["temp"];

            }
        }
        #endregion----------------------------------------------------------------------- 
    }
}