# AbpCodeBuilder

### 1. 开发环境
* VS2017 15.7.3
* .NET Framework 4.6.2
* DevExpress.v17.2.8
* **注：DevExpress不会提供，如需编译，请大家自行百度（你懂的）**

### 2. 说明
* 本工具基于AspNetZeroRadTool的模板
* **本工具要先有表，请大家先用code first生成表结构**
* 前端生成基于Vue，由于不会ng，暂时不提供ng模板的生成
* 前端模板基于Abp官网的[Vue后台模板](https://github.com/aspnetboilerplate/module-zero-core-template)

### 3. 教程

#### 3.1 生成列表

1. 勾选你需要生成的选项
2. 然后在User Interface上选择生成服务端还是前端（这一步必选，不然不会生成任何代码）
3. Authorization Provider和生成多语言的选项，都不会生成文件，自行复制代码到项目里吧
4. 多语言生成生成后还是要你自己填写，我只不过我让你少写一点而已= =

##### DapperMapper和Authorization Provider
这里需要注意的是`DapperMapper`和`Authorization Provider`两个选项

`DapperMapper`会生成如下所以的代码
```C#
public class ProductMapper : ClassMapper<Products.Product>
{
	public ProductMapper()
	{
		Table("Products");
		AutoMap();
	}
}
```
`Authorization Provider`会生成如下所以的代码，如果勾选了`Is Administration Permission`，生成代码会有不同

```C#

#region Products Admin Permissions 勾选了Is Administration Permission

public const string Pages_Administration_Products = "Pages.Administration.Products";
public const string Pages_Administration_Products_Create = "Pages.Administration.Products.Create";
public const string Pages_Administration_Products_Edit = "Pages.Administration.Products.Edit";
public const string Pages_Administration_Products_Delete = "Pages.Administration.Products.Delete";

#endregion

#region Products Admin Authorization Provider

var Products = context.CreateChildPermission(AppPermissions.Pages_Administration_Products, L("Products"));
Products.CreateChildPermission(AppPermissions.Pages_Administration_Products_Create, L("CreatingNewProduct"));
Products.CreateChildPermission(AppPermissions.Pages_Administration_Products_Edit, L("EditingProduct"));
Products.CreateChildPermission(AppPermissions.Pages_Administration_Products_Delete, L("DeletingProduct"));

#endregion

```

```C#

#region Products Permissions 没勾选Is Administration Permission

public const string Pages_Products = "Pages.Products";
public const string Pages_Products_Create = "Pages.Products.Create";
public const string Pages_Products_Edit = "Pages.Products.Edit";
public const string Pages_Products_Delete = "Pages.Products.Delete";

#endregion

#region Products Authorization Provider

var Products = context.CreateChildPermission(AppPermissions.Pages_Products, L("Products"));
Products.CreateChildPermission(AppPermissions.Pages_Products_Create, L("CreatingNewProduct"));
Products.CreateChildPermission(AppPermissions.Pages_Products_Edit, L("EditingProduct"));
Products.CreateChildPermission(AppPermissions.Pages_Products_Delete, L("DeletingProduct"));

#endregion

```

#### 3.2 实体信息

1. 自己的类前缀：这里的意思是，如果你的表有前缀，请填写到这里，会自动帮你替换。例如：`MIUsers`替换为`Users`，Abp里面的表，我已经自动处理，所以不用理会
2. BaseClass：这里有四个选项`Entity`、`AuditedEntity`、`CreationAuditedEntity`、`FullAuditedEntity`，选择对应的，会生成对应的Dto
	```C#
    Entity->EntityDto
    AuditedEntity->AuditedEntityDto
    以此类推
	```

#### 3.3 保存位置
1. 对于一开始不熟悉的，建议先玩玩预览生成，看看效果
2. 请别导出到自己项目的目录，以免覆盖了你的代码
